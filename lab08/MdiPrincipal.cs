﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab08
{
    public partial class MdiPrincipal : Form
    {
        public MdiPrincipal()
        {
            InitializeComponent();
        }

        private void personasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMntoPersonas frmMntoPersonas = new FrmMntoPersonas();
            frmMntoPersonas.MdiParent = this;
            frmMntoPersonas.Show();
        }

        private void departamentosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMntoDepartamentos frmMntoDepartamentos = new FrmMntoDepartamentos();
            frmMntoDepartamentos.MdiParent = this;
            frmMntoDepartamentos.Show();
        }

        private void cursosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMntoCursos frmMntoCursos = new FrmMntoCursos();
            frmMntoCursos.MdiParent = this;
            frmMntoCursos.Show();
        }

        private void salirDeLaAplicaciónToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void datosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCourseGrade frmCourseGrade = new FrmCourseGrade();
            frmCourseGrade.MdiParent = this;
            frmCourseGrade.Show();
        }
    }
}
