﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab08
{
    public partial class FrmCourseGrade : Form
    {
        SqlConnection con;
        DataSet ds = new DataSet();
        DataTable tableGrade = new DataTable();
        BindingManagerBase managerBase;
        SqlCommandBuilder builder;
        SqlDataAdapter adapter = new SqlDataAdapter();

        public FrmCourseGrade()
        {
            InitializeComponent();
        }

        private void FrmCourseGrade_Load(object sender, EventArgs e)
        {
            string str = "SERVER=.;DATABASE=School;Integrated Security=true;";
            con = new SqlConnection(str);
            string sql = "SELECT * from CourseGrade";
            SqlCommand cmd = new SqlCommand(sql, con);
            adapter.SelectCommand = cmd;
            adapter.Fill(ds, "CourseGrade");
            tableGrade = ds.Tables["CourseGrade"];
            dgvListadoCalificaciones.DataSource = tableGrade;

            txtEnrollmentID.DataBindings.Add("text", tableGrade, "EnrollmentID");
            txtCourseID.DataBindings.Add("text", tableGrade, "CourseID");
            txtStudentID.DataBindings.Add("text", tableGrade, "StudentID");
            txtGrade.DataBindings.Add("text", tableGrade, "Grade");

            managerBase = this.BindingContext[tableGrade];
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            managerBase.AddNew();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                managerBase.EndCurrentEdit();
                builder = new SqlCommandBuilder(adapter);
                adapter.Update(tableGrade);
                MessageBox.Show("Calificación agregada correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                managerBase.EndCurrentEdit();
                builder = new SqlCommandBuilder(adapter);
                adapter.Update(tableGrade);
                MessageBox.Show("Calificación modificada correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                managerBase.RemoveAt(managerBase.Position);
                builder = new SqlCommandBuilder(adapter);
                adapter.Update(tableGrade);
                MessageBox.Show("Calificación eliminada correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnPrimero_Click(object sender, EventArgs e)
        {
            managerBase.Position = 0;
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            managerBase.Position -= 1;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            managerBase.Position += 1;
        }

        private void btnUltimo_Click(object sender, EventArgs e)
        {
            managerBase.Position = managerBase.Count;
        }
    }
}
