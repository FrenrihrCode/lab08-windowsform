﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lab08
{
    public partial class FrmMntoDepartamentos : Form
    {
        SqlConnection con;
        DataSet ds = new DataSet();
        DataTable tableDepartment = new DataTable();
        BindingManagerBase managerBase;
        SqlCommandBuilder builder;
        SqlDataAdapter adapter = new SqlDataAdapter();

        public FrmMntoDepartamentos()
        {
            InitializeComponent();
        }

        private void FrmMntoDepartamentos_Load(object sender, EventArgs e)
        {
            string str = "SERVER=.;DATABASE=School;Integrated Security=true;";
            con = new SqlConnection(str);
            string sql = "SELECT * from Department";
            SqlCommand cmd = new SqlCommand(sql, con);
            adapter.SelectCommand = cmd;
            adapter.Fill(ds, "Department");
            tableDepartment = ds.Tables["Department"];
            dgvListadoDepartamentos.DataSource = tableDepartment;

            txtDepartmentID.DataBindings.Add("text", tableDepartment, "DepartmentID");
            txtName.DataBindings.Add("text", tableDepartment, "Name");
            txtBudget.DataBindings.Add("text", tableDepartment, "Budget");
            txtStartDate.DataBindings.Add("text", tableDepartment, "StartDate");
            txtAdministrator.DataBindings.Add("text", tableDepartment, "Administrator");

            managerBase = this.BindingContext[tableDepartment];
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            managerBase.AddNew();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                managerBase.EndCurrentEdit();
                builder = new SqlCommandBuilder(adapter);
                adapter.Update(tableDepartment);
                MessageBox.Show("Departamento agregado correctamente");
            } catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                managerBase.EndCurrentEdit();
                builder = new SqlCommandBuilder(adapter);
                adapter.Update(tableDepartment);
                MessageBox.Show("Departamento modificado correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                managerBase.RemoveAt(managerBase.Position);
                builder = new SqlCommandBuilder(adapter);
                adapter.Update(tableDepartment);
                MessageBox.Show("Departamento eliminado correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnPrimero_Click(object sender, EventArgs e)
        {
            managerBase.Position = 0;
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            managerBase.Position -= 1;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            managerBase.Position += 1;
        }

        private void btnUltimo_Click(object sender, EventArgs e)
        {
            managerBase.Position = managerBase.Count;
        }
    }
}
