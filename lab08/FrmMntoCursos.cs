﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace lab08
{
    public partial class FrmMntoCursos : Form
    {
        SqlConnection con;
        DataSet ds = new DataSet();
        DataTable tableCursos = new DataTable();
        BindingManagerBase managerBase;
        SqlCommandBuilder builder;
        SqlDataAdapter adapter = new SqlDataAdapter();
        public FrmMntoCursos()
        {
            InitializeComponent();
        }

        private void FrmMntoCursos_Load(object sender, EventArgs e)
        {
            string str = "SERVER=.;DATABASE=School;Integrated Security=true;";
            con = new SqlConnection(str);
            string sql = "SELECT * from Course";
            SqlCommand cmd = new SqlCommand(sql, con);
            adapter.SelectCommand = cmd;
            adapter.Fill(ds, "Course");
            tableCursos = ds.Tables["Course"];
            dgvListadoCursos.DataSource = tableCursos;

            txtCourseID.DataBindings.Add("text", tableCursos, "CourseID");
            txtTitulo.DataBindings.Add("text", tableCursos, "Title");
            txtCreditos.DataBindings.Add("text", tableCursos, "Credits");
            txtDepartamento.DataBindings.Add("text", tableCursos, "DepartmentID");

            managerBase = this.BindingContext[tableCursos];
        }

        private void btnNuevo_Click(object sender, EventArgs e)
        {
            managerBase.AddNew();
        }

        private void btnInsertar_Click(object sender, EventArgs e)
        {
            try
            {
                managerBase.EndCurrentEdit();
                builder = new SqlCommandBuilder(adapter);
                adapter.Update(tableCursos);
                MessageBox.Show("Curso agregado correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnModificar_Click(object sender, EventArgs e)
        {
            try
            {
                managerBase.EndCurrentEdit();
                builder = new SqlCommandBuilder(adapter);
                adapter.Update(tableCursos);
                MessageBox.Show("Curso modificado correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnEliminar_Click(object sender, EventArgs e)
        {
            try
            {
                managerBase.RemoveAt(managerBase.Position);
                builder = new SqlCommandBuilder(adapter);
                adapter.Update(tableCursos);
                MessageBox.Show("Curso eliminado correctamente");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void btnPrimero_Click(object sender, EventArgs e)
        {
            managerBase.Position = 0;
        }

        private void btnAnterior_Click(object sender, EventArgs e)
        {
            managerBase.Position -= 1;
        }

        private void btnSiguiente_Click(object sender, EventArgs e)
        {
            managerBase.Position += 1;
        }

        private void btnUltimo_Click(object sender, EventArgs e)
        {
            managerBase.Position = managerBase.Count;
        }
    }
}
